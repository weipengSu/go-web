package _interface

import (
	"fmt"
	"testing"
)

// 定义a为空接口
var a interface{}

var i int = 6
var s string = "hello world"

func A()  {
	a = i
	fmt.Println(a)
	a = s
	fmt.Println(a)
}

func TestNil(t *testing.T)  {
	A()
}