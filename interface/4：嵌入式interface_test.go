package _interface

import (
	"fmt"
	"testing"
)

type One interface {
	SayHello()
}

type Two interface {
	One
	Data()
}

type Human1 struct {
	name string
	age  int
}

func (h *Human1) SayHello() {
	fmt.Println(h.name, h.age)
}

type DataInfo struct {
}

func (data *DataInfo) Data() {
	fmt.Println("啦啦啦啦")
}

func TestOne(t *testing.T) {
	human := Human1{
		name: "swp",
		age:  10,
	}
	datas := DataInfo{}
	var i One

	i = &human

	i.SayHello()

	var s Two

	s = datas

	s.Data()

}
