package _interface

import (
	"fmt"
	"reflect"
	"strconv"
	"testing"
)

// commma-ok 断言

type Element interface{}

type List []Element

type Person struct {
	name string
	age  int
}

func (p Person) StringTd() string {
	return "(name: " + p.name + " - age: " + strconv.Itoa(p.age) + " years)"
}

func String1(list []Element) {
	for _, val := range list {
		if value, ok := val.(int); ok {
			fmt.Println(value, ":是int类型")
		} else if value, ok := val.(string); ok {
			fmt.Println(value, ": 是string 类型")
		} else {
			fmt.Println(value, ":其他类型")
		}
	}
}

func String2(list []Element) {
	for index, val := range list {
		switch value := val.(type) {
		case int:
			fmt.Sprintf("list[%d] is an int and its value is %d\n", index, value)
		case string:
			fmt.Sprintf("list[%d] is a string and its value is %s\n", index, value)
		case Person:
			fmt.Println(index, value)
		default:
			fmt.Sprintf("list[%d] is of a different type", index)
		}
	}

}

func TestString(t *testing.T) {
	list := make(List, 3)
	list[0] = 1
	list[1] = "2"
	list[2] = Person{
		name: "swp",
		age:  18,
	}
	fmt.Println(reflect.TypeOf(list))
	String1(list)

	String2(list)
	fmt.Println(reflect.TypeOf("1"))
}
