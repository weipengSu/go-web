package _interface

import (
	"fmt"
	"testing"
)

type Human struct {
	name  string
	age   int
	phone string
}

type Student struct {
	Human
	school string
	money  float64
}

type Employee struct {
	Human
	company string
	money   float64
}

// student 实现borrowmoney 方法
func (s *Student) BorrwoMoney(amount float64) {
	s.money += amount
}

// employee 实现 spendsalary方法
func (e *Employee) SpendSalary(amount float64) {
	e.money -= amount
}

// Human 实现 SayHi 方法
func (h *Human) SayHi() {
	fmt.Printf("Hi, I am %s you can call me on %s\n", h.name, h.phone)
}

// Human 实现 Sing 方法
func (h *Human) Sing(lyrics string) {
	fmt.Println("La la la la...", lyrics)
}

// Employee 重载 Human 的 SayHi 方法
func (e *Employee) SayHi() {
	fmt.Printf("Hi, I am %s, I work at %s. Call me on %s\n", e.name,
		e.company, e.phone)
}

type Men interface {
	SayHi()
	Sing(lyrics string)
}

func TestInteface(t *testing.T) {
	mike := Student{Human{"Mike", 25, "222-222-XXX"}, "MIT", 0.00}

	employee := Employee{
		Human: Human{
			name:  "swp",
			age:   100,
			phone: "188",
		},
		company: "无线电",
		money:   120.00,
	}

	// 定义Men 类型的变量
	var i Men

	// i 能存储 student
	i = &mike

	i.SayHi()

	i = &employee

	i.SayHi()

	data := make([]Men, 2)
	data[0], data[1] = &mike, &employee
	for _, value := range data{
		value.SayHi()
	}

}
