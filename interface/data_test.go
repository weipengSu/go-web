package _interface

import (
	"fmt"
	"testing"
)

type Man interface {
	Get() int
	Set(age int)
}

type S struct {
	Age int
}

func (s S) Get() int {
	return s.Age
}

func (s S) Set(age int) {
	s.Age = age
}

// 测试interface 空接口的使用
func Interface(a []interface{}) {
	for _, value := range a {
		fmt.Println("打印获得的value的值为:", value)
	}
}

func TestMam(t *testing.T) {
	a := []string{"111", "222", "333"}
	data := make([]interface{}, 3)
	for index, val := range a{
		data[index] = val
	}

	Interface(data)
	//s := S{Age:10}

	//var i Man

	//i = &s
	//
	//fmt.Println(s.Age)

}
