package _interface

import (
	"fmt"
	"testing"
)

func External_interface(list []interface{})  {
	for _, val := range list{
		if _, ok := val.(int); ok{
			fmt.Println("类型为int")
		}else if _, ok := val.(string); ok{
			fmt.Println("类型为string")
		}else{
			fmt.Println("other kind")
		}
	}
}

func Inward_interface(list []interface{})  {
	for _, value := range list{
		switch data := value.(type) {
		case int:
			fmt.Println("类型为int", data)
		case string:
			fmt.Println("类型为string", data)
		default:
			fmt.Println("other kind")
		}
	}
}

func TestInterface2(t *testing.T)  {
	a := make([]interface{}, 3)
	a[0] = 1
	a[1] = Person{
		name: "swp",
		age:  18,
	}
	a[2] = "striong"
	External_interface(a)
	Inward_interface(a)

}
