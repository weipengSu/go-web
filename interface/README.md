# go web

go web 编程

## Getting started

### inteface

```
    什么是 interface
    简单的说，interface 是一组 method 签名的组合，我们通过 interface 来定义对象的一组行为。
    interface 类型定义了一组方法，如果某个对象实现了某个接口的所有方法，则此对象就实现了此接口。
```
代码如下:
```
    
package main

import "fmt"

type Human struct {
    name string
    age int
    phone string
}

type Student struct {
    Human // 匿名字段
    school string
    loan float32
}

type Employee struct {
    Human // 匿名字段
    company string
    money float32
}

// Human 实现 SayHi 方法
func (h Human) SayHi() {
    fmt.Printf("Hi, I am %s you can call me on %s\n", h.name, h.phone)
}

// Human 实现 Sing 方法
func (h Human) Sing(lyrics string) {
    fmt.Println("La la la la...", lyrics)
}

// Employee 重载 Human 的 SayHi 方法
func (e Employee) SayHi() {
    fmt.Printf("Hi, I am %s, I work at %s. Call me on %s\n", e.name,
        e.company, e.phone)
    }

// Interface Men 被 Human, Student 和 Employee 实现
// 因为这三个类型都实现了这两个方法
type Men interface {
    SayHi()
    Sing(lyrics string)
}

func main() {
    mike := Student{Human{"Mike", 25, "222-222-XXX"}, "MIT", 0.00}
    paul := Student{Human{"Paul", 26, "111-222-XXX"}, "Harvard", 100}
    sam := Employee{Human{"Sam", 36, "444-222-XXX"}, "Golang Inc.", 1000}
    tom := Employee{Human{"Tom", 37, "222-444-XXX"}, "Things Ltd.", 5000}

    // 定义 Men 类型的变量i
    var i Men

    // i 能存储 Student
    i = mike
    fmt.Println("This is Mike, a Student:")
    i.SayHi()
    i.Sing("November rain")

    // i 也能存储 Employee
    i = tom
    fmt.Println("This is tom, an Employee:")
    i.SayHi()
    i.Sing("Born to be wild")

    // 定义了 slice Men
    fmt.Println("Let's use a slice of Men and see what happens")
    x := make([]Men, 3)
    // 这三个都是不同类型的元素，但是他们实现了 interface 同一个接口
    x[0], x[1], x[2] = paul, sam, mike

    for _, value := range x{
        value.SayHi()
    }
}
```
如果在定义的时候 使用的是指针, 那么在 使用 interface 的时候，也要带上地址:
```
    type Men interface {
        SayHi()
        Sing(lyrics string)
    }
    
    mike := Student{Human{"Mike", 25, "222-222-XXX"}, "MIT", 0.00}
    
    var i Men
    i = &mike
    i.SayHi()
```


###  空interface
```
空 interface
空 interface (interface {}) 不包含任何的 method，正因为如此，所有的类型都实现了空 interface。空 interface 对于描述起不到任何的作用 (因为它不包含任何的 method），但是空 interface 在我们需要存储任意类型的数值的时候相当有用，因为它可以存储任意类型的数值。它有点类似于 C 语言的 void* 类型。
一个函数把 interface {} 作为参数，那么他可以接受任意类型的值作为参数，如果一个函数返回 interface {}, 那么也就可以返回任意类型的值。是不是很有用啊
```
注意事项:
```
空interface 不能无脑使用，无脑使用要先判别类型
// 测试interface 空接口的使用
func Interface(a []interface{}) {
	for _, value := range a {
		fmt.Println("打印获得的value的值为:", value)
	}
}

func TestMam(t *testing.T) {
	a := []string{"111", "222", "333"}
	data := make([]interface{}, 3)
	for index, val := range a{
		data[index] = val
	}
}
```


###  interface 函数参数
```
    interface 的变量可以持有任意实现该 interface 类型的对象，我们可以通过定义 interface 参数，让函数接受各种类型的参数。
```


### interface 类型断言
如何判断一个interface 类型

在switch 内部, 直接用: 
```
 switch value := element.(type) {
      case int:
          fmt.Printf("list[%d] is an int and its value is %d\n", index, value)
      case string:
          fmt.Printf("list[%d] is a string and its value is %s\n", index, value)
    }
```

在外部环境中:
```
    for index, element := range list{
        if value, ok := element.(int); ok{}
        else if value, ok := element.(string); ok{}
        else: {}        
}
```