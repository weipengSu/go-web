# go web

go web 编程

### Getting started

###  web编程
```
http请求，当使用request的时候:
url := http://127.0.0.1:9090/hello/world?name=swp
     r *http.Request
获取 url:
      r.URL 即可获取 /hello/world?name=swp
解析参数，默认是不会解析的:
    r.ParseForm() 解析出来为: map[name:[swp]]
```

### web 工作方式的几个概念
以下均是服务器端的几个概念
```
Request：用户请求的信息，用来解析用户的请求信息，包括 post、get、cookie、url 等信息

Response：服务器需要反馈给客户端的信息

Conn：用户的每次请求链接

Handler：处理请求和生成返回信息的处理逻辑
```

### 监听端口
```
Go 是通过一个函数 ListenAndServe 来处理这些事情的，这个底层其实这样处理的：初始化一个 server 对象，然后调用了 net.Listen("tcp", addr)，
也就是底层用 TCP 协议搭建了一个服务，然后监控我们设置的端口。
```