package process_fuction

import (
	"fmt"
	"testing"
)

// 只有go一种循环表达式

func ForSess()  {
	for index := 1; index < 10; index ++ {
		fmt.Println(index)
	}
}

// range for
func ForRange(num [10]int)  {
	for index,value := range num{
		fmt.Println(index, value)
	}
}

func TestFor(t *testing.T)  {
	ForSess()
	var num = [10]int{1, 2, 3,4,5,6,7,8,9}
	ForRange(num)
}