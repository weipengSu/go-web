package process_fuction

import (
	"fmt"
	"testing"
)

// 传值
func add(a int) int {
	a = a + 1
	return a
}

// 传引用会改变值
func add1(a *int) int {
	* a = *a + 1
	return *a
}

func TestFunc1(t *testing.T) {
	x := 10
	add(x)
	fmt.Println(x)

	y := 10
	add1(&y)
	fmt.Println(y)
}
