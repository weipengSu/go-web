package process_fuction

import (
	"fmt"
	"os"
	"testing"
)

var user = os.Getenv("USER")

// 使用recover 恢复
func Funcinit()  {
	defer func() {
		if x := recover(); x != nil{
			fmt.Println("捕捉到了异常")
		}
	}()
	if user == ""{
		panic("no value for $USER")
	}
}

func TestFun4(t *testing.T)  {
	Funcinit()
}