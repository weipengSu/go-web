package process_fuction

import (
	"fmt"
	"testing"
)

func SumAndProduct(A, B int) (add, multiplied int) {
	return A + B, A * B
}

// arg 是一个slice
func ChangeParmas(arg ...int) {
	for index, value := range arg {
		fmt.Println(index, value)
	}
}

func TestFunc(t *testing.T) {
	var A, B int = 10, 5
	fmt.Println(SumAndProduct(A, B))
	//arg := make([]int, 10)
	ChangeParmas(1, 2, 3, 4, 5)
}
