# go web

go web 编程

## Getting started

### 流程控制if
```
    go 的if 可以跟条件表达式:
        if a := datasw(); a > 10{}
```

### 流程控制 for
    for 支持两种:
        1: for index := 1; index < 10; index ++ {}
        2: sum := [10]int{1,2,3,4,5}
            for index, value := range sum{}

### 流程控制switch
可以使用fallthrough 强制执行后续break 代码
 ```
    switch x {
    	case 1:
    		fmt.Println("i is equal to 1")
    	case 2:
    		fmt.Println("i is equal to 2")
    	default:
    		fmt.Println("other")
    	}
```


### 函数
```
    关键字 func 用来声明一个函数 funcName
    函数可以有一个或者多个参数，每个参数后面带有类型，通过 , 分隔
    函数可以返回多个值
    上面返回值声明了两个变量 output1 和 output2，如果你不想声明也可以，直接就两个类型
    如果只有一个返回值且不声明返回值变量，那么你可以省略 包括返回值 的括号
    如果没有返回值，那么就直接省略最后的返回信息
    如果有返回值， 那么必须在函数的外层添加 return 语句
```

不定长函数
不定长函数里面接受的是一个slice
``` 
func fuction(args ... int){
    for index, value := range args{}      
}


func main{
    function(1,2,3,4,5)
}
```

### 值和引用
```
传值:
    因为当我们调用 add 的时候，add 接收的参数其实是 x 的 copy，而不是 x 本身。
传引用:  & 取地址    * 取值
    我们知道，变量在内存中是存放于一定地址上的，修改变量实际是修改变量地址处的内存。只有 add1 函数知道 x 变量所在的地址，才能修改 x 变量的值。
所以我们需要将 x 所在地址 &x 传入函数，并将函数的参数的类型由 int 改为 *int，即改为指针类型，才能在函数中修改 x 变量的值。此时参数仍然是按 copy 传递的，只是 copy 的是一个指针

```


### defer
延迟执行
如果存在很多的defer的话, defer是采用后进先出的模式，例如:
    for i:= 0; i < 5; i++{ defer fmt.Println(i)}  输出 [4 3 2 1]
    
    
### panic recover
```
Panic

是一个内建函数，可以中断原有的控制流程，进入一个令人恐慌的流程中。当函数 F 调用 panic，函数 F 的执行被中断，但是 F 中的延迟函数会正常执行，然后 F 返回到调用它的地方。
在调用的地方，F 的行为就像调用了 panic。这一过程继续向上，直到发生 panic 的 goroutine 中所有调用的函数返回，此时程序退出。恐慌可以直接调用 panic 产生。也可以由运行时错误产生，例如访问越界的数组。

Recover

是一个内建的函数，可以让进入令人恐慌的流程中的 goroutine 恢复过来。recover 仅在延迟函数中有效。在正常的执行过程中，调用 recover 会返回 nil，并且没有其它任何效果。
如果当前的 goroutine 陷入恐慌，调用 recover 可以捕获到 panic 的输入值，并且恢复正常的执行。

```


###  main 和 init
```
Go 里面有两个保留的函数：init 函数（能够应用于所有的 package ）和 main 函数（只能应用于 package main）。这两个函数在定义时不能有任何的参数和返回值。虽然一个 package 里面可以写任意多个 init 函数，但这无论是对于可读性还是以后的可维护性来说，我们都强烈建议用户在一个 package 中每个文件只写一个 init 函数。

Go 程序会自动调用 init() 和 main()，所以你不需要在任何地方调用这两个函数。每个 package 中的 init 函数都是可选的，但 package main 就必须包含一个 main 函数。

程序的初始化和执行都起始于 main 包。如果 main 包还导入了其它的包，那么就会在编译时将它们依次导入。有时一个包会被多个包同时导入，那么它只会被导入一次（例如很多包可能都会用到 fmt 包，但它只会被导入一次，因为没有必要导入多次）。当一个包被导入时，如果该包还导入了其它的包，
那么会先将其它包导入进来，然后再对这些包中的包级常量和变量进行初始化，接着执行 init 函数（如果有的话），依次类推。等所有被导入的包都加载完毕了，就会开始对 main 包中的包级常量和变量进行初始化，然后执行 main 包中的 init 函数（如果存在的话），最后执行 main 函数。下图详细地解释了整个执行过程：

```

### import
1: 点操作  我们有时候会看到如下的方式导入包
```
import (
    . "fmt"
)
```
这个点操作的含义就是这个包导入之后在你调用这个包的函数时，你可以省略前缀的包名，也就是前面你调用的 fmt.Println ("hello world") 可以省略的写成 Println ("hello world")


2: 别名操作  别名操作顾名思义我们可以把包命名成另一个我们用起来容易记忆的名字
```
import(
    f "fmt"
)
```
别名操作的话调用包函数时前缀变成了我们的前缀，即 f.Println ("hello world")

3: _ 操作  这个操作经常是让很多人费解的一个操作符，请看下面这个 import
```
import (
    "database/sql"
    _ "github.com/ziutek/mymysql/godrv"
)
```
_操作其实是引入该包，而不直接使用包里面的函数，而是调用了该包里面的 init 函数
