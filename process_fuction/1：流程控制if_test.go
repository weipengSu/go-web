package process_fuction

import (
	"fmt"
	"testing"
)

// if
func iftest(x int) bool {
	if x > 10{
		return true
	}
	return false
}

// 支持跟条件表达式
func ifvariable()  string{
	if a := iftest(10); a == true{
		return "a > 10"
	} 	else{
		return "a <= 10"
	}
}

//  多条件用 if {}else if {} else{}
func TestIf(t *testing.T)  {
	iftest(10)
	fmt.Println(ifvariable())
}