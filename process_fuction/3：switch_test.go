package process_fuction

import (
	"fmt"
	"testing"
)

func switchtest(x int)  {
	switch x {
	case 1:
		fmt.Println("i is equal to 1")
	case 2:
		fmt.Println("i is equal to 2")
	default:
		fmt.Println("other")
	}
}

// 可以使用 fallthrough 强制执行break 后的代码
func switchfall(x int)  {
	switch x {
	case 1:
		fmt.Println("i is equal to 1")
		fallthrough
	case 2:
		fmt.Println("i is equal to 2")
		fallthrough
	default:
		fmt.Println("other")
	}
}


func TestSwitch(t *testing.T)  {
	switchtest(1)
	switchfall(1)
}