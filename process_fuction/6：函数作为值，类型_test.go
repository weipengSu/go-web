package process_fuction

import (
	"fmt"
	"testing"
)

type typeName func(int) bool

func isOdd(num int) bool {
	if num%2 == 0 {
		return false
	}
	return true
}

func isEvent(num int) bool {
	if num%2 != 0 {
		return false
	}
	return true
}

func filter(slice []int, f typeName) []int {
	var result []int
	for _, value := range slice {
		if f(value) {
			result = append(result, value)
		}
	}

	return result
}

func TestFunc3(t * testing.T) {
	slice := []int{1, 2, 3, 4, 5, 6}
	odd := filter(slice, isOdd)  // 函数当作值来传递
	event := filter(slice, isEvent)  // 函数当作值来传递
	fmt.Println(odd, event)
}
