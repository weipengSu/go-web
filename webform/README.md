# go web

go web 编程

## Getting started

### 处理表单的输入
```
r.ParseForm()       // 解析 url 传递的参数， 对于 POST 则解析响应包的主体（request body）

err := r.ParseForm()   // 解析 url 传递的参数，对于 POST 则解析响应包的主体（request body）
if err != nil {
   // handle error http.Error() for example
  log.Fatal("ParseForm: ", err)
}
// 请求的是登录数据，那么执行登录的逻辑判断
fmt.Println("username:", r.Form["username"])
fmt.Println("password:", r.Form["password"])


在postman 和 apifox 等测试工具可以进行测验，需要把 post body 里面的 x-www-form-urlencoded 里面填好参数即可
```


### 验证表单的输入
    开发 Web 的一个原则就是，不能信任用户输入的任何信息，所以验证和过滤用户的输入信息就变得非常重要，我们经常会在微博、新闻中听到某某网站被入侵了，
    存在什么漏洞，这些大多是因为网站对于用户输入的信息没有做严格的验证引起的，所以为了编写出安全可靠的 Web 程序，验证表单输入的意义重大。
    
### 必填字段
你想要确保从一个表单元素中得到一个值，例如前面小节里面的用户名，我们如何处理呢？Go 有一个内置函数 len 可以获取字符串的长度，
这样我们就可以通过 len 来获取数据的长度，例如：
        
        if len(r.Form["username"][0])==0{
            // 为空的处理
        }
