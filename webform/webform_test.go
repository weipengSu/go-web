package webform

import (
	"fmt"
	"log"
	"net/http"
	"testing"
)

func login(w http.ResponseWriter, r *http.Request)  {
	fmt.Println(r.Method) // 判断一个方法时什么类型   get/post/put/delete
	if r.Method == "GET"{
		fmt.Println("该方法为get")
	}else{
		err := r.ParseForm()
		if err != nil{
			log.Fatal("ParseForm:", err)
		}

		fmt.Println("username:", r.Form["username"])
		fmt.Println("password:", r.Form["password"])
	}

}

func TestLogin(t *testing.T)  {
	http.HandleFunc("/login", login)
	err := http.ListenAndServe(":9091", nil)
	if err != nil{
		log.Fatal("listerner:", err)
	}
}