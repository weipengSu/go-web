package importance

import (
	"fmt"
	"math"
	"testing"
)

type Rectangle struct {
	width, height float64
}

type Circle struct {
	redius float64
}

func (r Rectangle) area() float64 {
	return r.width * r.height
}

func (c Circle) area() float64 {
	return c.redius * c.redius * math.Pi
}

func TestComputer(t *testing.T) {
	r1 := Rectangle{1, 2}
	r2 := Circle{5}
	fmt.Println(r1.area())
	fmt.Println(r2.area())
}
