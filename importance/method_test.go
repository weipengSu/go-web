package importance

import (
	"fmt"
	"testing"
)

type Boy struct {
	name string
	age  int
}

func (b *Boy) Boy1()  {
	b.name = "哈哈哈"
}

func (b Boy) Boy2()  {
	b.name = "操"
}

func (b Boy) One()  {
	fmt.Println(b.name)
}


func TestBoy(t*testing.T)  {
	boy := Boy{
		name: "swp",
		age:  20,
	}

	boy.Boy1()
	boy.One()
}