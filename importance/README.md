# go web

go web 编程

## Getting started

### struct
struct 定义方式:
```
type Person struct{
    name string
    age int
}

1: var p Person  p.name = "swp" p.age = 18
2: p := Person{"name": "swp", "age": 18}
3: p := Person{"swp", 18}
4: p := new(Person)  p.name = "swp"  p.age = 18
```

struct 匿名操作:
```
    // struct 匿名字段
    type Human struct {
    	name   string
    	age    int
    	weight int
    }
    
    type Student struct {
    	Human      // 匿名字段,那么默认Student 就包含了Human的所有字段
    	speciality string
    }
    
    // 匿名调用
	mark := Student{
		Human:      Human{"mark", 25, 120},
		speciality: "computer scienck",
	}
	mark.name = "swp"
    // 还支持此匿名调用
    mark.Human = Human{name: "hahaha",age: 18, weight: 180}

    // 如果多个匿名同一个相同的字段: 则先取本结构体，然后按照匿名结构体定义排序来写
    或者直接用  mark.Human = Human{name: "hahaha",age: 18, weight: 180}  此匿名调用
```


### 面向对象
```
method 不仅仅只作用struct 上，他还可以定义在任何你自定义的类型，内置类型，struct 等各种类型上面， 自定义类型不单单指的是 struct， struct 只是一种比较特殊的类型而已。

例如:
    1: type ages int
    2: type money float
    3: type months map[string]int
    m := months {"January":31,"February":28,"December":31,}

注意事项: 在定义的时候 会遇到 type months map[string]int    var  months map[string]int, 前者只是定义一个别名， 后者是定义对象
    例如: 
    1: type months map[string]int
        data := months {"January":31,"February":28,"December":31,}  定义别名，后续可以使用
    2: var months map[string]int
        months["data"] = 1  # 定义对象，后续只能往里面填充值
```

### method 也支持继承和重写
```
    结构体在匿名操作的时候，也会继承匿名操作所拥有的method
    也可以重写该method，详情请见 对应的 源码  method_继承, method_重写
```