package importance

import (
	"fmt"
	"testing"
)

// struct 类型和实体
type person struct {
	name string
	age  int
}

var P person

func personfunc() {
	// 初始化定义1
	P.name = "su"
	P.age = 18
	// 定义2
	p1 := person{
		name: "swp",
		age:  20,
	}

	// 定义3
	p2 := person{"swp1", 23}

	//定义4
	p3 := new(person)
	p3.name = "哈哈哈"
	p3.age = 20
	fmt.Println(P, p1, p2, *p3)
}

// 比较两个人的年龄，返回年龄大的那个人，并且返回年龄差
// struct 也是传值的
func Older(p1, p2 person) (person, int) {
	if p1.age > p2.age {
		return p1, p1.age - p2.age
	}
	return p2, p2.age - p1.age
}

func TestOlder(t *testing.T) {
	p1 := person{"swp", 23}
	p2 := person{"swp", 18}
	fmt.Println(Older(p1, p2))
}

// struct 匿名字段
type Human struct {
	name   string
	age    int
	weight int
}

type Student struct {
	Human      // 匿名字段,那么默认Student 就包含了Human的所有字段
	speciality string
}

func StudentNiMing()  {
	mark := Student{
		Human:      Human{"mark", 25, 120},
		speciality: "computer scienck",
	}

	fmt.Println("age:", mark.age)
	mark.name = "swp"
	fmt.Println(mark)

	mark.Human = Human{
		name:   "hahaha",
		age:    18,
		weight: 180,
	}

	fmt.Println(mark)
}

func TestNiMing(t *testing.T)  {
	StudentNiMing()
}

func Test1(t *testing.T) {
	personfunc()
	//fmt.Println(P)
}
