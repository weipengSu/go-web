package importance

import (
	"fmt"
	"testing"
)

type Human1 struct {
	name  string
	age   int
	phone string
}

type Student1 struct {
	Human1 // 匿名字段
	school string
}

type Employee struct {
	Human1  // 匿名字段
	company string
}

// Human 定义method
func (h *Human1) SayHi() {
	fmt.Printf("Hi, I am %s you can call me on %s\n", h.name, h.phone)
}

// 继承
func TestMds(t *testing.T) {
	a := Student1{Human1{
		name:  "swp",
		age:   0,
		phone: "111",
	}, "lala"}

	b := Employee{
		Human1: Human1{name: "swp2",
			age:   10,
			phone: "222",},
		company: "youyu",
	}

	a.SayHi()
	b.SayHi()
}


