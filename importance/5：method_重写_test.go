package importance

import (
	"fmt"
	"testing"
)

type Human2 struct {
	name string
	age  int
}

type Student2 struct {
	Human2
	school string
}

type Employee2 struct {
	Human2
	company string
}

func (h *Human2) SayHello() string {
	return fmt.Sprintf("%s hello world %d", h.name, h.age)
}

func (e *Employee2) SayHello() string {
	return fmt.Sprintf("%s Hello World, 你好 %d", e.name, e.age)
}

func TestFunc2(t *testing.T) {
	student := Student2{
		Human2: Human2{"我是", 18},
		school: "上学啦",
	}

	employee := Employee2{
		Human2:  Human2{"swp", 20},
		company: "邵阳学院",
	}

	fmt.Println(student.SayHello(), employee.SayHello())
}
