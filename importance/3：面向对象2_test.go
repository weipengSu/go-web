package importance

import (
	"fmt"
	"testing"
)

type age int
type money float64

type months map[string]int

// 二  测试
const (
	White = iota
	Black
	Blue
	Red
	Yellow
)

type Color byte

type Box struct {
	width, height, depth float64
	color                Color
}

type BoxList []Box

func (b Box) Volume() float64 {
	return b.width * b.height * b.depth
}

func (b *Box) SetColor(c Color) {
	b.color = c
}

func (bl BoxList) PaintItBlack() {
	for i := range bl {
		bl[i].SetColor(Black)
	}
}

func (c Color) String () string {
	data := [...]string{"White", "Black", "Blue", "Red", "Yellow"}
	return data[c]
}


func TestMethod(t *testing.T) {
	//age = 10
	boxes := BoxList{
		Box{
			width:  1,
			height: 1,
			depth:  1,
			color:  White,
		},
		Box{
			width:  2,
			height: 2,
			depth:  2,
			color:  Black,
		},
	}

	boxes.PaintItBlack()
	for _, value := range boxes{
		fmt.Println("获得的颜色为:", value.color.String())
	}
}
