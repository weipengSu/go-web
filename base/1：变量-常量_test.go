package base

import (
	"fmt"
	"testing"
)

// 变量定义
// 定义多个变量
var name1, name2, name3 string

// 给变量赋值
var name4 int = 1

// 也可以直接写为
var name5, name6 = 1, 5

func Test_variable(t *testing.T) {
	fmt.Println(name5, name6)
}

// 定义常量

const const_data int = 1

func Test_Const(t *testing.T) {
	fmt.Println("const_data:", const_data)
}
