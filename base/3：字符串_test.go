package base

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
)

// 字符串声明
var frenchHello string  // 声明变量为字符串的一般方法

var emptyString string = ""  // 声明字符串并初始化为空字符串


// 注意点，在go中字符串是不可变的, 例如
var string_data = "hello"

func TestString(t *testing.T)  {
	fmt.Println(string_data)
	// 此方法不可取
	//string_data[0] := "c"

	c := []byte(string_data)  // 将字符串 s 转换为[]byte 类型
	fmt.Println(c)
	c[0] = 'c'
	fmt.Println(string(c))
}

var string1 string = "hello"
var string2 string = "world"

func bufferjoin()  {
	var buffer bytes.Buffer
	buffer.WriteString(string1)
	buffer.WriteString(",")
	buffer.WriteString(string2)
	fmt.Println(buffer.String())
}

func BenchJoin()  {
	fmt.Println(strings.Join([]string{string1, string2}, ","))
}

// 字符串拼接
func TestStringJoin(t* testing.T)  {
	bufferjoin()
	BenchJoin()
}