package base

import (
	"fmt"
	"reflect"
	"testing"
)

// array

//var array[n]int   n 长度, int 类型

// 声明方式 1:
var arr [10]int // 声明一个int 类型，长度为10的数组

// 声明方式 2:
func statement() {
	a := [3]int{1, 2, 3}   // 声明一个长度为3的数组
	b := [10]int{1, 2, 3}  // 声明一个长度为10 的数组，其中前三个初始元素为1， 2，3, 其他默认为0
	c := [...]int{4, 5, 6} // 使用...，不声明长度，go会自动根据元素个数来计算长度

	// 多维数组
	d := [2][4]int{{1,2,3,4}, {5,6,7}}
	fmt.Println(a, b, c, d)
	fmt.Println("长度为:", len(b))
}

// slice 切片
// slice 并不是真正意义上的动态数组，而是一个引用类型。slice 总是指向一个底层 array，slice 的声明也可以像 array 一样，只是不需要长度。
// slice声明

var fslice []int // 和声明array 一样，只是少了长度, slice 能够很好的适配切片

//如果从一个数组里面直接获取 slice，可以这样 ar[:]，因为默认第一个序列是 0，第二个是数组的长度，即等价于 ar[0:len(ar)]

// 声明方式二
func statement_slice()  {
	slice_data := []byte{'a', 'b', 'c', 'd'}
	fmt.Println(slice_data[1:3])
}

func slice_array()  {
	arr := [...]int{1,2,3,4,5,6}
	fmt.Println(arr, reflect.TypeOf(arr))
	slice_data := arr[:]
	fmt.Println(slice_data, reflect.TypeOf(slice_data))
}

// map 声明方式
var numbers map[string]int

func mapstatement()  {
	numbers := make(map[string]int)
	numbers["one"] = 1
	fmt.Println(numbers)
}

func operate_map()  {
	map1 := map[string]float32{
		"c": 5,
		"Go": 4.5,
		"python": 4.5,
		"c++": 2,
	}

	if value, ok := map1["c++"]; ok{
		fmt.Println(value)
	}else{
		fmt.Println("该值不存在于map中")
	}

	delete(map1, "c")
	fmt.Println("获得的map为:", map1)
}


func TestArray(t *testing.T) {
	statement()
	statement_slice()
	slice_array()
	mapstatement()
	operate_map()
}
