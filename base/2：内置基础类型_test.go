package base

import (
	"fmt"
	"testing"
)

// bool 类型
var isActive bool // 全局变量声明

var enable, disable = true, false // 忽略类型的声明

// 注重点: go不同类型之间不能进行运算， 例如 int32, int64
func different_kind_computer() {
	const data1 int32 = 1
	const data2 int64 = 2
	fmt.Println(int64(data1) + data2)
}

// error类型
//Go 内置有一个 error 类型，专门用来处理错误信息，Go 的 package 里面还专门有一个包 errors 来处理错误


func Test_different_kind(t *testing.T) {
	different_kind_computer()
}
