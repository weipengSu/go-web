# go web

go web 编程

## Getting started

## 变量和常量区别
```
变量定义时使用 var, 常量使用const, 在声明变量常量时, ":=" 只用于函数内部，
不能在外部使用
```

### 内置基础类型
```
1: bool 类型
    var isActive bool // 全局类型声明
    var enable, disable = true, false // 忽略类型的声明
2：error 类型
    Go 内置有一个 error 类型，专门用来处理错误信息，Go 的 package 里面还专门有一个包 errors 来处理错误：
    err := errors.New("emit macho dwarf: elf header corrupted")

注意事项：
    go 在进行不同类型操作的时候，必须转成同一类型才能进行，否则会error
```

### 字符串
字符串修改

go中的字符串是不可变的，如果需要修改，那么需要使用[]byte
```
1：转换为 []byte类型
    var string_data = "hello"
    c := []byte(string_data)  // 将字符串 s 转换为[]byte 类型
    // [104 101 108 108 111]
    c[0] = 'c'
    fmt.Println(string(c))  // cello

2: 利用字符串的拼接，切片
    var string_data = "hello"
    data := "c" + string_data[1:]
```

字符串拼接方法
```
1: 直接拼接(+):
    hello + "," + world
    golang 里面的字符串都是不可变的，每次运算都会产生一个新的字符串，所以会产生很多临时的无用的字符串，不仅没有用，还会给 gc 带来额外的负担，所以性能比较差
2: fmt.Sprintf()
    fmt.Sprintf("%s, %s", hello, world)
    内部使用 []byte 实现，不像直接运算符这种会产生很多临时的字符串，但是内部的逻辑比较复杂，有很多额外的判断，还用到了 interface，所以性能也不是很好
3: strings.join()
    strings.Join([]string{string1, string2}, ",")
    join会先根据字符串数组的内容，计算出一个拼接之后的长度，然后申请对应大小的内存，一个一个字符串填入，在已有一个数组的情况下，这种效率会很高，但是本来没有，去构造这个数据的代价也不小
4: buffer []byte
    var buffer bytes.Buffer
    buffer.WriteString(hello)
    buffer.WriteString(",")
    buffer.WriteString(world)
    fmt.Println(buffer.String())
    这个比较理想，可以当成可变字符使用，对内存的增长也有优化，如果能预估字符串的长度，还可以用 buffer.Grow() 接口来设置 capacity
```

字符串拼接分析
```
    1: 在已有字符串数组的场合，使用 strings.Join() 能有比较好的性能
    2: 在一些性能要求较高的场合，尽量使用 buffer.WriteString() 以获得更好的性能
    3: 性能要求不太高的场合，直接使用运算符，代码更简短清晰，能获得比较好的可读性
    4: 如果需要拼接的不仅仅是字符串，还有数字之类的其他需求的话，可以考虑 fmt.Sprintf()
```

### array 数组
array 数组定义方式:
```
    var arr [10]int // 声明一个int 类型，长度为10的数组
    a := [3]int{1, 2, 3}   // 声明一个长度为3的数组
    b := [10]int{1, 2, 3}  // 声明一个长度为10 的数组，其中前三个初始元素为1， 2，3, 其他默认为0
    c := [...]int{4, 5, 6} // 使用...，不声明长度，go会自动根据元素个数来计算长度
    
    // 多维数组
    d := [2][4]int{{1,2,3,4}, {5,6,7}}

注意事项:
    不能数组越界，会error，如果数组里面为读取到值，会默认读到0
```


### slice 切片
slice 声明方式:
```
    slice 并不是真正意义上的动态数组，而是一个引用类型。slice 总是指向一个底层 array，slice 的声明也可以像 array 一样，只是不需要长度。
    slice声明

    var fslice []int // 和声明array 一样，只是少了长度, slice 能够很好的适配切片
    slice_data := []byte{'a', 'b', 'c', 'd'}
```
   
   
### map 
map 声明方式:
```
map 也就是 Python 中字典的概念，它的格式为 map[keyType]valueType
```
一: 声明方式：
```
    1: var numbers map[string]int
    2: make(map[string]int)
```

二: map使用
```
    1: map 初始化: 
        rating := map[string]float32{"C":5, "Go":4.5, "Python":4.5, "C++":2 }
    2: 判断map中是否有该值:
        if value, ok := map[data]; ok{}
    3: 删除map:
        delete(rating, "c")
``` 

注意点:
```
    1：map 是无序的，每次打印出来的 map 都会不一样，它不能通过 index 获取，而必须通过 key 获取
    2：map 的长度是不固定的，也就是和 slice 一样，也是一种引用类型
    3：内置的 len 函数同样适用于 map，返回 map 拥有的 key 的数量
    4：map 的值可以很方便的修改，通过 numbers["one"]=11 可以很容易的把 key 为 one 的字典值改为 11
    5：在 Go 中，没有值可以安全地进行并发读写，它不是 thread-safe，在多个 go-routine 存取时，必须使用 mutex lock 机制
```


### make new
```
make, new 操作
new 只用于内存分配, make 也是用于内存分配，但针对的对象是 channel, map, slice
```