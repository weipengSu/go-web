package base

import (
	"fmt"
	"testing"
)

// make, new 操作
// new 只用于内存分配, make 也是用于内存分配，但针对的对象是 channel, map, slice

type Student struct {
	name string
	age  int
}

func TestSum(t *testing.T) {
	var s Student
	//s = new(Student)
	s.name = "啦啦啦"
	fmt.Println(s)
}
