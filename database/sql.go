package database

import "time"

type Blog struct {
	Model
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type Model struct {
	ID         uint      `json:"id"`
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time `json:"update_time"`
}
