package supervene

import (
	"fmt"
	"testing"
)

func data(n int, c chan int)  {
	for i := 0; i < n; i ++{
		c <- i
		if i == 3{
			close(c)
			break
		}
	}
	//close(c)
}

func TestChan(t *testing.T)  {
	var n int = 10
	c := make(chan int, 5)
	go data(n, c)
	for i := range c{
		fmt.Println(i)
	}
}