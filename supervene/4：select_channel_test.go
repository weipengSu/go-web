package supervene

import (
	. "fmt"
	"os"
	"testing"
	"time"
)

func FuncChan(n int, ch chan int) {
	for i := 0; i < n; i++ {
		ch <- i
	}
	close(ch)
}

func TestFunc(t *testing.T) {
	ch := make(chan int, 10)
	go FuncChan(10, ch)
	for {
		select {
		case v := <-ch:
			Println("打印获得的值为:", v)
		case <-time.After(5 * time.Second):
			Println("timeout")
			break
		default:
			Println("exit")
			os.Exit(-1)
		}

	}
}
