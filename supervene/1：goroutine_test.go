package supervene

import (
	"fmt"
	"testing"
)

// goroutine

func Say(s string) {
	for i := 0; i < 50000; i++ {
		//runtime.Gosched()
		fmt.Println(s)
	}
}


func TestGoroutine(t *testing.T) {
	go Say("Hello")
	Say("World")
}
