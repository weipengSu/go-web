# go web

go web 编程

## Getting started

### goroutine
```
    goroutine 是 Go 并行设计的核心。goroutine 说到底其实就是协程，但是它比线程更小，十几个 goroutine 可能体现在底层就是五六个线程，Go 语言内部帮你实现了这些 goroutine 之间的内存共享。执行 goroutine 只需极少的栈内存 (大概是 4~5 KB)，当然会根据相应的数据伸缩。也正因为如此，可同时运行成千上万个并发任务。goroutine 比 thread 更易用、更高效、更轻便。
    
    goroutine 是通过 Go 的 runtime 管理的一个线程管理器。goroutine 通过 go 关键字实现了，其实就是一个普通的函数。

    runtime.Gosched () 表示让 CPU 把时间片让给别人，下次某个时候继续恢复执行该 goroutine。

    默认情况下，在 Go 1.5 将标识并发系统线程个数的 runtime.GOMAXPROCS 的初始值由 1 改为了运行环境的 CPU 核数。
    
    但在 Go 1.5 以前调度器仅使用单线程，也就是说只实现了并发。想要发挥多核处理器的并行，需要在我们的程序中显式调用 runtime.GOMAXPROCS (n) 告诉调度器同时使用多个线程。GOMAXPROCS 设置了同时运行逻辑代码的系统线程的最大数量，并返回之前的设置。如果 n < 1，不会改变当前设置。
  
```
协程注意事项:
    使用协程的话，协程与后面执行的 函数是同步的，并不是从上到下执行的。
```
例如:
    func fibonacci(n int, c chan int) {
    	x, y := 1, 1
    	for i := 0; i < n; i++ {
    		c <- x
    		x, y = y, x + y
    	}
    	close(c)
    }
    
    func TestBuffer(t *testing.T) {
    	c := make(chan int, 10)
    	go fibonacci(cap(c), c)
    	for i := range c {
    		fmt.Println(i)
    	}
    }
在此过程中 go fibonacci(cap(c), c) 和下文的 for循环是同步的, 一个生产一个消费。
```


### channel
```
goroutine 运行在相同的地址空间，因此访问共享内存必须做好同步。那么 goroutine 之间如何进行数据的通信呢，Go 提供了一个很好的通信机制 channel。channel 可以与 Unix shell 中的双向管道做类比：
可以通过它发送或者接收值。这些值只能是特定的类型： channel 类型。定义一个 channel 时，也需要定义发送到 channel 的值的类型。注意，必须使用 make 创建 channel：
```
channel 使用 make来新建
```
    make(chan int)
    make(chan string)
    make(chan interface{})
```
channel 通过操作符 <- 来接收和发送数据
```
    ch <- v    // 发送 v 到 channel ch.
    v := <-ch  // 从 ch 中接收数据，并赋值给v
```
channel 可分为有符号类型和无符号类型
```
无符号类型:
    默认情况下，channel 接收和发送数据都是阻塞的，除非另一端已经准备好，这样就使得 Goroutines 同步变的更加的简单，而不需要显式的 lock。所谓阻塞，也就是如果读取（value := <-ch）它将会被阻塞，直到有数据接收。其次，任何发送（ch<-5）将会被阻塞，直到数据被读出。
无缓冲 channel 是在多个 goroutine 之间同步很棒的工具。

有符号类型:
    可以指定Buffer,不需要同时生成，同时消费。可以使用buffer暂存。
```

channel 如何防止阻塞
```
两种方式:
一: 检测close()
    v, ok := <-ch 测试 channel 是否被关闭。如果 ok 返回 false，那么说明 channel 已经没有任何数据并且已经被关闭。
    记住应该在生产者的地方关闭 channel，而不是消费的地方去关闭它，这样容易引起 panic

select 默认是阻塞的，只有当监听的 channel 中有发送或接收可以进行时才会运行，
当多个 channel 都准备好的时候，select 是随机的选择一个执行的。
```

### runtime goroutine
```
runtime 包中有几个处理 goroutine 的函数：

    Goexit

退出当前执行的 goroutine，但是 defer 函数还会继续调用

    Gosched

让出当前 goroutine 的执行权限，调度器安排其他等待的任务运行，并在下次某个时候从该位置恢复执行。

    NumCPU

返回 CPU 核数量

    NumGoroutine

返回正在执行和排队的任务总数

    GOMAXPROCS

用来设置可以并行计算的 CPU 核数的最大值，并返回之前的值

```