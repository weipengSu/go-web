package supervene

import (
	"testing"
)

//channel 通过操作符 <- 来接收和发送数据
//ch <- v    // 发送 v 到 channel ch.
//v := <-ch  // 从 ch 中接收数据，并赋值给v

// channel 定义
func sum(a []int, c chan int) {
	total := 0
	for _, v := range a {
		total += v
	}

	c <- total
	c <- 100
	c <- 200
	close(c)
}

//func ()  {
//
//}

// 针对这种情况，是主协程执行完退出了，子协程还未执行


func TestChannel(t *testing.T) {
	a := []int{1, 2, 3, 4, 5, 6}
	ch := make(chan int)
	go sum(a, ch)
	//// 无buffer channel
	//go sum(a, ch)
	////fmt.Println(<-ch)
	//for data := range ch{
	//	fmt.Println(data)
	//}

}
